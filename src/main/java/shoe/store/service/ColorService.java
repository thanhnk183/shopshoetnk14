package shoe.store.service;

import shoe.store.entity.Color;

public interface ColorService extends BaseService<Color, Long> {

}
