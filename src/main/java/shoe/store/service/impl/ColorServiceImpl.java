package shoe.store.service.impl;

import org.springframework.stereotype.Service;

import shoe.store.entity.Color;
import shoe.store.service.ColorService;

@Service
public class ColorServiceImpl extends BaseServiceImpl<Color, Long> implements ColorService {

}
